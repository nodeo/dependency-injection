<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace DI\Container;

use DI\AbstractBinder;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;

class ContainerBuilder
{
    private AbstractBinder $binder;

    private bool $autoWiring = false;

    #[Pure] private function __construct(AbstractBinder $binder)
    {
        $this->binder = $binder;
    }

    #[Pure] public static function new(AbstractBinder $binder): self
    {
        return new ContainerBuilder($binder);
    }

    public function withAutoWiring(): self
    {
        $this->autoWiring = true;

        return $this;
    }

    public function build(): ContainerInterface
    {
        $this->binder->configure();

        return new Container(
            $this->binder->getDependencies(),
            $this->getOptions()
        );
    }

    #[ArrayShape(['autoWiring' => "bool"])]
    #[Pure] private function getOptions(): array
    {
        return [
            'autoWiring' => $this->autoWiring
        ];
    }
}

<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace DI\Container;

use JetBrains\PhpStorm\Pure;

interface ContainerInterface
{
    public function get(string $id);

    #[Pure] public function has(string $id);
}

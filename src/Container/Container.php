<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace DI\Container;

use DI\Exceptions\NotFoundException;
use DI\Mappers\ScalarMapper;
use DI\Resolver;
use JetBrains\PhpStorm\Pure;

class Container implements ContainerInterface
{
    private array $binds;

    private array $options;

    private Resolver $resolver;

    #[Pure] public function __construct(array $binds, array $options = [])
    {
        $binds[ContainerInterface::class] = new ScalarMapper(
            $this
        ); // Add the container

        $this->binds = $binds;
        $this->options = $options;

        $this->resolver = new Resolver($binds);
    }

    public function get(string $id)
    {
        if ($this->has($id)) {
            return $this->resolver->resolve($id);
        } elseif ($this->options['autoWiring']) {
            // Dynamical resolving
            return $this->resolver->resolve($id);
        } else {
            throw new NotFoundException($id);
        }
    }

    #[Pure] public function has(string $id): bool
    {
        return array_key_exists($id, $this->binds);
    }
}

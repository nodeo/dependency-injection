<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace DI;

use DI\Mappers\ClassMapper;
use DI\Mappers\ScalarMapper;
use InvalidArgumentException;
use JetBrains\PhpStorm\ExpectedValues;

abstract class AbstractBinder
{
    private array $dependencies = [];

    abstract public function configure(): void;

    public function getDependencies(): array
    {
        return $this->dependencies;
    }

    protected function bindToInstance(
        string $definition,
        object $instance
    ): void {
        if (!class_exists($definition) && !interface_exists($definition)) {
            throw new InvalidArgumentException(
                sprintf("%s must exist", $definition)
            );
        }

        if (key_exists($definition, $this->dependencies)) {
            throw new InvalidArgumentException(
                sprintf(
                    "%s has already be bonded with %s",
                    $definition,
                    $this->dependencies[$definition]
                )
            );
        }

        $this->dependencies[$definition] = new ScalarMapper($instance);
    }

    protected function bindToScalar(string $definition, $value): void
    {
        if (key_exists($definition, $this->dependencies)) {
            throw new InvalidArgumentException(
                sprintf(
                    "%s has already be bonded with %s",
                    $definition,
                    $this->dependencies[$definition]
                )
            );
        }

        $this->dependencies[$definition] = new ScalarMapper($value);
    }

    protected function bind(string $definition, string $implementation): void
    {
        $this->bindClass(
            $definition,
            $implementation,
            ClassMapper::RE_INSTANTIABLE
        );
    }

    protected function bindAsSingleton(
        string $definition,
        string $implementation
    ): void {
        $this->bindClass($definition, $implementation, ClassMapper::SINGLETON);
    }

    private function bindClass(
        string $definition,
        string $implementation,
        #[ExpectedValues([
            ClassMapper::RE_INSTANTIABLE,
            ClassMapper::SINGLETON
        ])] int $status
    ) {
        if (
            !(class_exists($definition) && interface_exists(
                $definition
            )) && !class_exists($implementation)
        ) {
            throw new InvalidArgumentException(
                sprintf("%s and %s must exist", $definition, $implementation)
            );
        }

        if (key_exists($definition, $this->dependencies)) {
            throw new InvalidArgumentException(
                sprintf(
                    "%s has already be bonded with %s",
                    $definition,
                    $this->dependencies[$definition]
                )
            );
        }

        $this->dependencies[$definition] = new ClassMapper(
            $implementation,
            $status
        );
    }
}

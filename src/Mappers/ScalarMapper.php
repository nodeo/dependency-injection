<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace DI\Mappers;

use JetBrains\PhpStorm\Pure;

class ScalarMapper extends AbstractMapper
{
    #[Pure] public function __construct(mixed $value)
    {
        parent::__construct($value);
    }
}

<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace DI\Exceptions;

use JetBrains\PhpStorm\Pure;
use RuntimeException;

class NotFoundException extends RuntimeException
{
    #[Pure] public function __construct(string $id)
    {
        parent::__construct(
            sprintf(
                "The key '%s' could not be resolved",
                $id
            )
        );
    }
}

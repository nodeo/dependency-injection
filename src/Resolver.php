<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace DI;

use DI\Exceptions\NotFoundException;
use DI\Mappers\ClassMapper;
use InvalidArgumentException;
use JetBrains\PhpStorm\Pure;
use LogicException;
use ReflectionClass;
use ReflectionException;
use ReflectionNamedType;
use RuntimeException;

class Resolver
{
    private array $binds;

    private array $dependencies;

    #[Pure] public function __construct(array $binds)
    {
        $this->binds = $binds;
        $this->dependencies = [];

        foreach ($binds as $id => $bind) {
            if (!($bind instanceof ClassMapper)) {
                $value = $bind->getValue();
                $this->dependencies[$id] = $value;
            }
        }
    }

    public function resolve(string $id): mixed
    {
        if (array_key_exists($id, $this->dependencies)) {
            return $this->dependencies[$id];
        } else {
            if (!class_exists($id) && !interface_exists($id)) {
                throw new NotFoundException($id);
            }

            try {
                $implementation = array_key_exists(
                    $id,
                    $this->binds
                ) ? $this->binds[$id]->getValue() : $id;
                $class = new ReflectionClass($implementation);

                if (!$class->isInstantiable()) {
                    throw new InvalidArgumentException(
                        sprintf(
                            '"%s" is not instantiable',
                            $id
                        )
                    );
                }

                $constructor = $class->getConstructor();
                $parameters = [];

                if ($constructor) {
                    foreach ($constructor->getParameters() as $parameter) {
                        $parameterType = $parameter->getType();

                        if (!$parameterType && !($parameterType instanceof ReflectionNamedType)) {
                            throw new LogicException(
                                sprintf(
                                    'Cannot instantiate %s because %s is not typed',
                                    $id,
                                    $parameter->getName()
                                )
                            );
                        }

                        $parameters[] = $this->resolve(
                            $parameterType->getName()
                        );
                    }
                }

                $instance = $class->newInstance(...$parameters);

                if (array_key_exists($id, $this->binds)) {
                    $mapper = $this->binds[$id];

                    if (
                        $mapper instanceof ClassMapper &&
                        $mapper->getStatus() === ClassMapper::SINGLETON
                    ) {
                        // Put the instance on the dependencies array if the mapper is marked as a singleton
                        $this->dependencies[$id] = $instance;
                    }
                }

                return $instance;
            } catch (ReflectionException $exception) {
                throw new RuntimeException($exception->getMessage());
            }
        }
    }
}

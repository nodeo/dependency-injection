<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace Tests;

use DI\AbstractBinder;
use JetBrains\PhpStorm\Pure;

class MockBinder extends AbstractBinder
{
    private B $b;

    /**
     * MockBinder constructor.
     */
    #[Pure] public function __construct()
    {
        $this->b = new B();
    }

    public function configure(): void
    {
        $this->bindToScalar("DB_HOST", "localhost");
        $this->bindToScalar("DB_PORT", 3309);

        $this->bindAsSingleton(IA::class, A::class);

        $this->bindToInstance(B::class, $this->b);

        $this->bind(IC::class, C::class);
    }

    /**
     * @return B
     */
    public function getB(): B
    {
        return $this->b;
    }
}

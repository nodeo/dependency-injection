<?php

/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Tests;

use JetBrains\PhpStorm\Pure;

class A implements IA
{
    private B $b;

    /**
     * A constructor.
     * @param B $b
     */
    #[Pure] public function __construct(B $b)
    {
        //$b->sayHello();
        $this->b = $b;
    }

    public function sayHello(): string
    {
        return 'Hello from A !';
    }

    /**
     * @return B
     */
    public function getB(): B
    {
        return $this->b;
    }
}

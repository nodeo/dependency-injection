<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace Tests;

use DI\Container\ContainerBuilder;
use DI\Exceptions\NotFoundException;
use PHPUnit\Framework\TestCase;

class DependencyTest extends TestCase
{
    public function testAutoWiring()
    {
        $containerBuilder = ContainerBuilder::new(new MockBinder());
        $container = $containerBuilder
            ->withAutoWiring()
            ->build();

        self::assertFalse($container->has(D::class));

        $d1 = $container->get(D::class);
        $d2 = $container->get(D::class);

        self::assertFalse($d1 === $d2);
        self::assertEquals($d1, $d2);
    }

    public function testToScalarBinding()
    {
        $containerBuilder = ContainerBuilder::new(new MockBinder());
        $container = $containerBuilder->build();

        self::assertTrue($container->has('DB_HOST'));
        self::assertEquals('localhost', $container->get('DB_HOST'));

        self::assertFalse($container->has('db_port'));
        self::assertEquals(3309, $container->get('DB_PORT'));
    }

    public function testToInstanceBinding()
    {
        $binder = new MockBinder();
        $containerBuilder = ContainerBuilder::new($binder);
        $container = $containerBuilder
            ->withAutoWiring()
            ->build();

        self::assertTrue($container->has(B::class));

        $b1 = $binder->getB();
        $b2 = $container->get(B::class);
        $b3 = $container->get(B::class);

        self::assertEquals('Hello from B !', $b1->sayHello());

        $d = $container->get(D::class);

        $b4 = $d->getA()->getB();

        self::assertTrue($b1 === $b2 && $b2 === $b3 && $b3 === $b4);
    }

    public function testToSingletonBinding()
    {
        $containerBuilder = ContainerBuilder::new(new MockBinder());
        $container = $containerBuilder
            ->withAutoWiring()
            ->build();

        self::assertTrue($container->has(IA::class));
        self::assertFalse($container->has(A::class));

        $a1 = $container->get(IA::class);
        $a2 = $container->get(IA::class);

        self::assertInstanceOf(IA::class, $a1);
        self::assertInstanceOf(A::class, $a1);

        $d = $container->get(D::class);

        $a3 = $d->getA();

        self::assertTrue($a1 === $a2 && $a2 === $a3);
    }

    public function testRaisesExceptionWhenNoAutoWiring()
    {
        $this->expectException(NotFoundException::class);

        $containerBuilder = ContainerBuilder::new(new MockBinder());
        $container = $containerBuilder->build();

        $container->get(D::class);
    }
}
